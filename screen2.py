#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("2step")
window.geometry('800x600')

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)

def first_screen():
    first_frame.pack()

    tk.Label(first_frame, text="This is the 1st step").pack()

    button_next = tk.Button(first_frame, text='Next', command=second_screen)
    button_next.pack()

def second_screen(): 
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="This is the 2nd step").pack()

first_screen()
window.mainloop()
