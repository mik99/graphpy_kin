#!/usr/bin/env python3

import tkinter as tk

window = tk.Tk()
window.title("L'inspi")
window.geometry("800x600")

init_frame = tk.Frame(window)
init_frame.pack()

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)
third_frame = tk.Frame(window)
fourth_frame = {}

def first_screen():
    first_frame.pack()
    tk.Label(first_frame, text="Obligé d'être populaire, que j'ai la côte, mais j'en ai pas l'air").pack()

    button_next = tk.Button(first_frame, text='Next', command=second_screen)
    button_next.pack()

def second_screen():
    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="Le monde est plein de zéros, ça fera pas de nous des heureux").pack()

    button_next = tk.Button(second_frame, text='Next', command=lambda: third_screen(second_frame))
    button_next.pack()

def third_screen(previous_frame):
    previous_frame.destroy()
    third_frame.pack()
    tk.Label(third_frame, text="chante avec une sale mine, j'ai fait mes premiers pas truc truc").pack()

    button_next = tk.Button(third_frame, text='Next', command=lambda: fourth_screen(third_frame))
    button_next.pack()

def fourth_screen(previous_frame):
    previous_frame.destroy()
    fourth_frame['f'] = tk.Frame(window)
    fourth_frame['f'].pack()
    mon_label = tk.Label(fourth_frame['f'], text="j'cultive des trucs mais jsuis pas agri").pack()
    fourth_frame['tresor_cache'] = "3 btc"
    print(fourth_frame['tresor_cache'])

first_screen()
window.mainloop()

